-- Local API
local CreateFrame = CreateFrame
local C_Timer = C_Timer
local debugstack = debugstack
local UIParent = UIParent
local GetTime = GetTime
local GetCursorPosition = GetCursorPosition
local GetAddOnMetadata = GetAddOnMetadata
local GameFontHighlightSmall = GameFontHighlightSmall
local InterfaceOptions_AddCategory = InterfaceOptions_AddCategory

-- Addon
WhereIsMyMouse = CreateFrame("Frame", "WhereIsMyMouse", UIParent)
local Addon = WhereIsMyMouse

-- Debug mode
Addon.errors = false

-- Addon states
Addon.INVISIBLE = 0
Addon.FADE_IN   = 1
Addon.VISIBLE   = 2
Addon.FADE_OUT  = 3

local ZOOM = 3

-- Default values
Addon.state    = Addon.INVISIBLE
Addon.alphaMax = 0.75
Addon.size     = 384 * UIParent:GetEffectiveScale()
Addon.speed    = 0.100
Addon.toggle   = false

-- Restore values from save file
function Addon:Init()
  Addon.alphaMax = savesChar.alphaMax or Addon.alphaMax
  Addon.size     = savesChar.size     or Addon.size
  Addon.speed    = savesChar.speed    or Addon.speed
  Addon.toggle   = savesChar.toggle   or Addon.toggle

  Addon.panel.alpha:SetValue(math.floor(Addon.alphaMax * 100))
  Addon.panel.size:SetValue(Addon.size)
  Addon.panel.speed:SetValue(math.floor(Addon.speed * 1000))
  Addon.panel.toggle:SetChecked(Addon.toggle)
end

-- Key binding strings, matched to Bindings.xml at runtime by the wow client
BINDING_HEADER_WHEREISMYMOUSE = "Where Is My Mouse"
BINDING_NAME_SHOWMEMYMOUSE = "Show Me My Mouse"

Addon:SetFrameStrata("TOOLTIP")
Addon:SetFrameLevel(10000)

-- Slash command
SLASH_WHEREISMYMOUSE1 = "/whereismymouse"
SLASH_WHEREISMYMOUSE2 = "/wimm"
function SlashCmdList.WHEREISMYMOUSE(msg, editBox)
  InterfaceAddOnsList_Update()
  InterfaceOptionsFrame_OpenToCategory(Addon.panel)
end

-- Center texture
Addon:SetWidth(Addon.size * ZOOM)
Addon:SetHeight(Addon.size * ZOOM)
Addon.tex = Addon:CreateTexture()
Addon.tex:SetTexture("Interface/AddOns/WhereIsMyMouse/whereismymouse.tga")
Addon.tex:SetAllPoints(Addon)

-- Left/Right/Top/Bottom panels
Addon.l = CreateFrame("Frame", Addon:GetName() .. "Left",   Addon)
Addon.r = CreateFrame("Frame", Addon:GetName() .. "Right",  Addon)
Addon.t = CreateFrame("Frame", Addon:GetName() .. "Top",    Addon)
Addon.b = CreateFrame("Frame", Addon:GetName() .. "Bottom", Addon)

Addon.l:SetPoint("TOPRIGHT",    Addon, "TOPLEFT")
Addon.l:SetPoint("BOTTOMRIGHT", Addon, "BOTTOMLEFT")
Addon.l:SetPoint("LEFT",        nil,   "LEFT")

Addon.r:SetPoint("TOPLEFT",    Addon, "TOPRIGHT")
Addon.r:SetPoint("BOTTOMLEFT", Addon, "BOTTOMRIGHT")
Addon.r:SetPoint("RIGHT",      nil,   "RIGHT")

Addon.t:SetPoint("TOPLEFT",  nil,   "TOPLEFT")
Addon.t:SetPoint("TOPRIGHT", nil,   "TOPRIGHT")
Addon.t:SetPoint("BOTTOM",   Addon, "TOP")

Addon.b:SetPoint("BOTTOMLEFT",  nil,   "BOTTOMLEFT")
Addon.b:SetPoint("BOTTOMRIGHT", nil,   "BOTTOMRIGHT")
Addon.b:SetPoint("TOP",         Addon, "BOTTOM")

Addon.l.t = Addon:CreateTexture()
Addon.r.t = Addon:CreateTexture()
Addon.t.t = Addon:CreateTexture()
Addon.b.t = Addon:CreateTexture()

Addon.l.t:SetColorTexture(0, 0, 0, 1)
Addon.r.t:SetColorTexture(0, 0, 0, 1)
Addon.t.t:SetColorTexture(0, 0, 0, 1)
Addon.b.t:SetColorTexture(0, 0, 0, 1)

Addon.l.t:SetAllPoints(Addon.l)
Addon.r.t:SetAllPoints(Addon.r)
Addon.t.t:SetAllPoints(Addon.t)
Addon.b.t:SetAllPoints(Addon.b)

--------------------------------------------------------------------------------

-- Handle state changes
function Addon:ChangeState(newState)
  if Addon.state == Addon.INVISIBLE then
    if newState == Addon.FADE_IN then
      -- Go from invisible to fading in
      Addon.state = newState
      Addon.alpha = 0
      Addon.start = GetTime()
      Addon.stop  = Addon.start + Addon.speed
      Addon:Show()
      Addon:SetAlpha(Addon.alpha)
      -- Enable per frame updates
      Addon:SetScript("OnUpdate", Addon.OnUpdate)
    end
  elseif Addon.state == Addon.FADE_IN then
    if newState == Addon.VISIBLE then
      -- Now fully visible, after fading in
      Addon.state = newState
      Addon.alpha = 1
    elseif newState == Addon.FADE_OUT then
      -- User let go of key early, fadeout
      Addon.state = newState
      Addon.start = GetTime() - (1 - Addon.alpha) * Addon.speed
      Addon.stop  = Addon.start + Addon.speed
    end
  elseif Addon.state == Addon.VISIBLE then
    if newState == Addon.FADE_OUT then
      -- User let go of key, fadeout
      Addon.state = newState
      Addon.start = GetTime()
      Addon.stop  = Addon.start + Addon.speed
    end
  elseif Addon.state == Addon.FADE_OUT then
    if newState == Addon.FADE_IN then
      -- User pressed key while fading out, fade back in
      Addon.state = newState
      Addon.start = GetTime() - Addon.alpha * Addon.speed
      Addon.stop  = Addon.start + Addon.speed
    elseif newState == Addon.INVISIBLE then
      -- Now fully invisible after fading out
      Addon.state = newState
      Addon.alpha = 0
      Addon:Hide()
      -- Disable per frame updates
      Addon:SetScript("OnUpdate", nil)
    end
  end
end

--------------------------------------------------------------------------------

function Addon:OnEventP(event, ...)
  if event == "ADDON_LOADED" then
    local name = select(1, ...)
    if name ~= Addon:GetName() then
      return
    end
    
    -- Addon initial state
    if not savesChar then
      savesChar = {
        alphaMax = Addon.alphaMax,
        size     = Addon.size,
        speed    = Addon.speed,
        toggle   = Addon.toggle
      }
    end
    Addon:Init()
    if Addon.errors then
      print(Addon:GetName() .. " is loaded in debug mode!")
    end
  end
end

--
-- Executes event code in a protected call.
-- @param ... Event and event arguements.
--
function Addon:OnEvent(...)
  Addon.xpcall(Addon.OnEventP, Addon, ...)
end

Addon:SetScript("OnEvent", Addon.OnEvent)
Addon:RegisterEvent("ADDON_LOADED")

--------------------------------------------------------------------------------

function Addon:OnUpdateP(...)
  local uiScale, x, y = UIParent:GetEffectiveScale(), GetCursorPosition()
  Addon:SetPoint("CENTER", nil, "BOTTOMLEFT", x / uiScale, y / uiScale)
  local now = GetTime()
  if Addon.state == Addon.VISIBLE then
    return
  elseif Addon.state == Addon.FADE_IN then
    if now >= Addon.stop then
      Addon:ChangeState(Addon.VISIBLE)
      now = Addon.stop
      Addon.alpha = 1
    else
      Addon.alpha = (now - Addon.start) / (Addon.speed > 0 and Addon.speed or 0.001)
    end
  elseif Addon.state == Addon.FADE_OUT then
    if now >= Addon.stop then
      Addon:ChangeState(Addon.INVISIBLE)
      now = Addon.stop
      Addon.alpha = 0
    else
      Addon.alpha = (Addon.stop - now) / (Addon.speed > 0 and Addon.speed or 0.001)
    end
  end

  Addon:SetAlpha(Addon.alpha * Addon.alphaMax)
  -- Scale the size of the aperature based on the current alpha value
  -- Lower the alpha, the larger the aperature. Higher, the smaller.
  Addon:SetWidth(
    Addon.size*UIParent:GetEffectiveScale() * (1+ZOOM - (ZOOM*Addon.alpha))
  )
  Addon:SetHeight(Addon:GetWidth())
end

function Addon:OnUpdate(...)
  Addon.xpcall(Addon.OnUpdateP, ...)
end

--------------------------------------------------------------------------------

-- Interface Options helper function
function Addon.CreateSliderAndEditBox(name, parent, sibling, xoff, yoff)
  local slider =
  CreateFrame(
    "Slider",
    parent:GetName() .. name,
    parent,
    "OptionsSliderTemplate"
  )
  slider:SetPoint(
    "TOPLEFT", sibling:GetName() or parent:GetName(), "BOTTOMLEFT", xoff, yoff
  )
  slider:SetWidth(500)
  slider:SetHeight(15)
  slider:SetOrientation("HORIZONTAL")
  slider.Text:SetTextColor(1, 0.82, 0)
  local function OnValueChanged(self, value)
    -- Set the edit box to match the slider value
    if slider:GetValueStep() >= 1 then
      -- Use whole numbers only, remove all decimals
      value = value - math.fmod(value, self:GetValueStep())
    end
    self.box:SetText(value)
  end
  slider:SetScript("OnValueChanged", OnValueChanged)

  slider.box = CreateFrame(
    "EditBox", slider:GetName() .. "Box", slider, "InputBoxTemplate"
  )
  slider.box:SetPoint("TOP", slider:GetName(), "BOTTOM", 0, -15)
  slider.box:SetAutoFocus(false)
  slider.box:SetFontObject(GameFontHighlightSmall)
  slider.box:SetHeight(14)
  slider.box:SetWidth(70)
  slider.box:SetJustifyH("CENTER")
  slider.box:EnableMouse(true)
  slider.box:SetMaxLetters(4)
  
  local function FocusLost(self)
    self:ClearFocus()
    self:ClearHighlightText()
    C_Timer.After(
      0.05,
      function()
        -- Restore field value, as enter/tab wasn't used
        local value = slider:GetValue()
        OnValueChanged(slider, value)
      end
    )
  end
  slider.box:SetScript("OnEditFocusLost", FocusLost)

  slider.box:SetScript(
    "OnEscapePressed",
    function(self)
      self:ClearFocus()
      self:SetText(tostring(slider:GetValue()))
    end
  )
  
  local function EnterTab(self)
    self:ClearFocus()
    self:ClearHighlightText()
    -- Ensure only a valid number was entered
    local value = tonumber(self:GetText()) or slider:GetValue()
    slider:SetValue(value)
  end
  
  slider.box:SetScript("OnEnterPressed", EnterTab)
  slider.box:SetScript("OnTabPressed", EnterTab)
  return slider
end

-- Interface options panel
Addon.panel = CreateFrame("Frame", Addon:GetName() .. "Panel", UIParent)
Addon.panel.name = Addon:GetName()
Addon.panel:Hide() -- Needed or child objects won't get OnShow first view
Addon.panel.title =
Addon.panel:CreateFontString(
  Addon.panel:GetName() .. "Title", "OVERLAY", "GameFontNormalLarge"
)
Addon.panel.title:SetPoint("TOPLEFT", Addon.panel:GetName(), "TOPLEFT", 15, -15)
Addon.panel.title:SetText(
  Addon:GetName() .. " " .. GetAddOnMetadata("WhereIsMyMouse", "Version")
)

-- Toggle
Addon.panel.toggle =
CreateFrame(
  "CheckButton",
  Addon:GetName() .. "Toggle",
  Addon.panel,
  "ChatConfigCheckButtonTemplate"
)
Addon.panel.toggle:SetPoint(
  "TOPLEFT", Addon.panel.title:GetName(), "BOTTOMLEFT", 0, -15
)
Addon.panel.toggle.Text:SetText(" Toggle instead of hold")

-- Aperature alpha
Addon.panel.alpha =
Addon.CreateSliderAndEditBox(
  Addon.panel:GetName() .. "Alpha", Addon.panel, Addon.panel.toggle, 0, -20
)
Addon.panel.alpha:SetMinMaxValues(0, 100)
Addon.panel.alpha:SetValue(math.floor(Addon.alphaMax * 100))
Addon.panel.alpha:SetValueStep(1)
Addon.panel.alpha.Low:SetText("0")
Addon.panel.alpha.High:SetText("100")
Addon.panel.alpha.Text:SetText("Aperature Alpha")
Addon.panel.alpha.tooltipText =
"How opaque to make the screen when showing the mouse."
Addon.panel.alpha.box:SetText(Addon.alphaMax * 100)

-- Aperature size
Addon.panel.size =
Addon.CreateSliderAndEditBox(
  Addon.panel:GetName() .. "Size", Addon.panel, Addon.panel.alpha, 0, -65
)
Addon.panel.size:SetMinMaxValues(128, 1024)
Addon.panel.size:SetValue(Addon.size)
Addon.panel.size:SetValueStep(1)
Addon.panel.size.Low:SetText("128px")
Addon.panel.size.High:SetText("1024px")
Addon.panel.size.Text:SetText("Aperature Size")
Addon.panel.size.tooltipText = "How much screen to hide when showing the mouse."
Addon.panel.size.box:SetText(Addon.size)

-- Aperature speed
Addon.panel.speed =
Addon.CreateSliderAndEditBox(
  Addon.panel:GetName() .. "Speed", Addon.panel, Addon.panel.size, 0, -65
)
Addon.panel.speed:SetMinMaxValues(0, 1000)
Addon.panel.speed:SetValue(math.floor(Addon.speed * 1000))
Addon.panel.speed:SetValueStep(1)
Addon.panel.speed.Low:SetText("0ms")
Addon.panel.speed.High:SetText("1000ms")
Addon.panel.speed.Text:SetText("Aperature Speed")
Addon.panel.speed.tooltipText = "How fast to show the mouse."
Addon.panel.speed.box:SetText(Addon.speed * 1000)

-- Panel OK and Cancel
Addon.panel.okay =
function(self)
  -- Save changed settings
  Addon.alphaMax = Addon.panel.alpha:GetValue() / 100
  Addon.size     = Addon.panel.size:GetValue()
  Addon.speed    = Addon.panel.speed:GetValue() / 1000
  Addon.toggle   = Addon.panel.toggle:GetChecked()
  
  savesChar.alphaMax = Addon.alphaMax
  savesChar.size     = Addon.size
  savesChar.speed    = Addon.speed
  savesChar.toggle   = Addon.toggle
end

Addon.panel.cancel =
function(self)
  -- Restore all settings
  Addon.panel.alpha:SetValue(Addon.alphaMax * 100)
  Addon.panel.size:SetValue(Addon.size)
  Addon.panel.speed:SetValue(Addon.speed * 1000)
  Addon.panel.toggle:SetChecked(Addon.toggle)
end

-- Add to interface options
InterfaceOptions_AddCategory(Addon.panel)

--------------------------------------------------------------------------------

--
-- Performs a protected call that will, for errors, have a stacktrace.
-- @param f   The function to call.
-- @param ... The function parameters.
--
function Addon.xpcall(f, ...)
  local args = {...}
  local success, values = xpcall(
    function()
      return {f(unpack(args))}
    end,
    function(msg)
      return {msg, debugstack()}
    end
  )
  if not values or type(values) ~= "table" then
    return
  end
  if not success and Addon.errors then
    print(values[1], values[2])
  end
  return unpack(values)
end

-- /console scriptErrors 1
-- /console scriptErrors 0
