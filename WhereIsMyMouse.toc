## Interface: 40400
## Title: WhereIsMyMouse
## Author: Hindenburg
## Version: 1.1.3
## Notes: Shows the player where their mouse is on the screen.
## SavedVariablesPerCharacter: savesChar

main.lua
